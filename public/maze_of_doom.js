/**
 * Depth rendering adapted from:
 * https://github.com/mrdoob/three.js/blob/master/examples/webgl_depth_texture.html
 * 
 * Font from:
 * https://ff.static.1001fonts.net/s/u/subway-ticker.regular.ttf
 */

// Global variables
//#region global vars
var camera, scene, renderer;
// target is for rendering the nokia screen at true 84x48 res
// postFXTarget will apply further processing effects for nokia screen effect
// Finally postFXTarget will be scaled up to match decided screen resolution.
var target, postFXTarget;
var postScene, postCamera;
var presentationScene, presentationCamera;
var myGeomGroup, gatekeeperGroup, activeTextMesh;

/** Dimensions of nokia screen in pixels. */
const nokiaRTSize = new THREE.Vector2(84, 48);
/** Dimensions of actual visible nokia screen element in pixels. */
var myScreenSize = getMaxPresentationSize();

/** Movement vector from current control inputs. */
var moveTarget = new THREE.Vector2(0.0, 0.0);
/** Text speed multiplier when scrolling text across the screen. */
const textScrollRate = 1.0;

/** Object tracking the state of the current match. */
var gameStats = {
	/** Whether movement is allowed. (position and rotation) */
	bAllowMovement: true,
	/** Whether made died text is active.on the screen. */
	bHasMadeDiedText: false,
	/** Whether player has finished viewing introduction text. */
	bFinishedInto: false,
	/** Movement speed multiplier for positional displacement. */
	moveSpeed: 0.1,
	/** Whether player has requested to interact this frame. */
	bRequestInteract: false,
	/** Whether player has finished viewing death method text. */
	bFinishedDeathMethod: false
};

const startText = {
	text: (
		"You Died"
		+ "       "
		+ "       "
		+ "The only way to leave purgatory is to complete this mini game."
		+ "       "
		+ "Once you escape this maze, your place in the afterlife will be secured."
	),
	length: 500
};

const deathMethodText = {
	text: (
		"How did you die?"
		+ "       "
		+ "You dropped dead from exhaustion while attempting "
		+ "to escape a maze, of course!"
	),
	length: 320
};

//#endregion

// Simple functions
//#region simple func
var lerp = (a, b, bias) => a + (b - a) * bias;
var nearlyZero = x => -0.01 < x && x < 0.01;
var between = (x, a, b) => a < x && x < b;
var around = (x, v) => between(x, v - 0.5, v + 0.5);
//#endregion

// Start everything.
(() => {
	init();
	animate();
})()

function init() {

	renderer = new THREE.WebGLRenderer({ canvas: document.querySelector('canvas') });

	renderer.setPixelRatio(window.devicePixelRatio);
	renderer.setSize(myScreenSize.x, myScreenSize.y);

	//

	camera = new THREE.PerspectiveCamera(55, myScreenSize.x / myScreenSize.y, 0.01, 50);
	camera.position.x = -5;
	camera.position.y = 5.5;
	camera.position.z = 20;

	// Create a multi render target with Float buffers
	target = new THREE.WebGLRenderTarget(nokiaRTSize.x, nokiaRTSize.y);
	target.texture.format = THREE.RGBFormat;
	target.texture.minFilter = THREE.NearestFilter;
	target.texture.magFilter = THREE.NearestFilter;
	target.texture.generateMipmaps = false;
	target.stencilBuffer = false;
	target.depthBuffer = true;
	target.depthTexture = new THREE.DepthTexture();
	target.depthTexture.type = THREE.UnsignedShortType;

	postFXTarget = new THREE.WebGLRenderTarget(nokiaRTSize.x, nokiaRTSize.y);
	postFXTarget.texture.format = THREE.RGBFormat;
	postFXTarget.texture.minFilter = THREE.NearestFilter;
	postFXTarget.texture.magFilter = THREE.NearestFilter;
	postFXTarget.texture.generateMipmaps = false;
	postFXTarget.stencilBuffer = false;
	postFXTarget.depthBuffer = true;
	postFXTarget.depthTexture = new THREE.DepthTexture();
	postFXTarget.depthTexture.type = THREE.UnsignedShortType;


	// Our scene
	scene = new THREE.Scene();
	mySetupScene();

	// Setup post-processing step
	setupPost();

	// Setup the final scene that scales and present the image to the user.
	setupPresentation();

	var listener = new THREE.AudioListener();
	var audio = new THREE.Audio(listener);
	var mediaElement = new Audio('loop.mp3');
	mediaElement.loop = true;
	mediaElement.play();
	audio.setMediaElementSource(mediaElement);
}

function setupPost() {

	// Setup post processing stage
	postCamera = new THREE.OrthographicCamera(- 1, 1, 1, - 1, 0, 1);
	var postMaterial = new THREE.ShaderMaterial({
		vertexShader: document.querySelector('#post-vert').textContent.trim(),
		fragmentShader: document.querySelector('#post-frag').textContent.trim(),
		uniforms: {
			cameraNear: { value: camera.near },
			cameraFar: { value: camera.far },
			tDiffuse: { value: target.texture },
			tDepth: { value: target.depthTexture },
			ditherAmt: { value: 1.0 }
		}
	});
	var postPlane = new THREE.PlaneBufferGeometry(2, 2);
	var postQuad = new THREE.Mesh(postPlane, postMaterial);
	postScene = new THREE.Scene();
	postScene.add(postQuad);

}

function setupPresentation() {
	presentationCamera = new THREE.OrthographicCamera(- 1, 1, 1, - 1, 0, 1);
	var mat = new THREE.ShaderMaterial({
		vertexShader: document.querySelector('#post-vert').textContent.trim(),
		fragmentShader: document.querySelector('#post-frag').textContent.trim(),
		uniforms: {
			cameraNear: { value: camera.near },
			cameraFar: { value: camera.far },
			tDiffuse: { value: postFXTarget.texture },
			tDepth: { value: target.depthTexture },
			ditherAmt: { value: 0.0 }

		}
	});
	var geom = new THREE.PlaneBufferGeometry(2, 2);
	var plane = new THREE.Mesh(geom, mat);
	presentationScene = new THREE.Scene();
	presentationScene.add(plane);
}

function mySetupScene() {
	myGeomGroup = new THREE.Group();

	surfaceMat = new THREE.MeshBasicMaterial({ color: 0x00ff00, side: THREE.DoubleSide });

	// Create the floor.
	var floorGeom = new THREE.PlaneBufferGeometry(5000, 5000);
	var floor = new THREE.Mesh(floorGeom, surfaceMat);
	floor.rotation.x = -Math.PI / 2;
	floor.position.y = -3; // Lower the floor.
	myGeomGroup.add(floor);

	// Create the walls.
	var wallGeom = new THREE.PlaneBufferGeometry(50, 50);

	// north south is on yz plane
	var addWallNS = (x, y) => {
		var wall = new THREE.Mesh(wallGeom, surfaceMat);
		wall.position.x = -25 + (50 * x);
		wall.position.y = 5;
		wall.position.z = -50 * y;
		wall.rotation.x = Math.PI / 2;
		wall.rotation.y = Math.PI / 2;
		myGeomGroup.add(wall);
		return wall;
	}

	// east west is on xy plane
	var addWallEW = (x, y) => {
		var wall = new THREE.Mesh(wallGeom, surfaceMat);
		wall.position.x = 50 * x;
		wall.position.y = 5;
		wall.position.z = 25 - 50 * y;
		myGeomGroup.add(wall);
		return wall;
	}

	// Starting corridor three way enclosure.
	addWallEW(0, 0);
	addWallNS(0, 0);
	addWallNS(1, 0);

	// Right and forward turning point.
	addWallEW(1, 1);
	addWallNS(0, 1);
	addWallEW(1, 2);
	addWallNS(0, 2);
	addWallNS(1, 2);

	// Continue down the right turn.
	addWallEW(2, 1);
	addWallNS(2, 2);
	addWallEW(2, 2);
	addWallEW(3, 1);
	addWallEW(4, 1);
	addWallEW(4, 2);
	addWallEW(5, 1);

	// Continue down the leftward turn a long way.
	addWallNS(6, 1);
	addWallNS(5, 2);
	addWallNS(6, 2);
	addWallNS(5, 3);
	addWallNS(6, 3);
	addWallNS(5, 4);
	addWallNS(6, 4);
	addWallNS(5, 5);
	addWallNS(6, 5);
	addWallEW(5, 6);

	// Continuing forward two grid steps, with rightmost empty.
	addWallNS(0, 3);
	addWallNS(1, 3);
	addWallNS(0, 4);

	// Taking a right turn two steps.
	addWallEW(0, 5);
	addWallEW(1, 5);
	addWallEW(1, 4);
	addWallEW(2, 5);
	addWallEW(2, 4);

	// Making a right turn again towards the other path.
	addWallEW(3, 5);
	addWallNS(4, 4);
	addWallNS(3, 3);
	addWallNS(4, 3);

	// Head straight down twice but leave gap for a left turn.
	addWallNS(3, 2);
	addWallNS(4, 2);

	scene.add(myGeomGroup);
}

function animate() {

	requestAnimationFrame(animate);

	if (gameStats.bAllowMovement) {
		// Move if allowed. Sure, rotation is movement.
		camera.translateZ(moveTarget.x * -0.5 * gameStats.moveSpeed);
		camera.rotation.y += moveTarget.y * -0.06;
	}

	// Check if reached mini game starting point.
	if (around(camera.position.z, 8)
		&& (!gameStats.bFinishedInto || gameStats.bRequestInteract)) {
		// Stop moving!
		gameStats.bFinishedInto = false;
		gameStats.bAllowMovement = false;

		if (!nearlyZero(camera.rotation.y)) {
			// Rotate camera towards center.
			let oldRot = camera.rotation.y;
			let newRot = lerp(oldRot, 0, 0.2);
			camera.rotation.y = newRot;
		} else if (!gameStats.bHasMadeDiedText) {
			// Show You Died text and hide people.
			gameStats.bHasMadeDiedText = true;
			gatekeeperGroup.visible = false;

			makeText(startText.text, text => {
				// Save in global variable for later access.
				activeTextMesh = text;
			});
		} else if (activeTextMesh.position.x > -startText.length) {
			// Animate shown text.
			let oldPos = activeTextMesh.position.x;
			let newPos = oldPos - (0.25 * textScrollRate);
			activeTextMesh.position.x = newPos;
		} else {
			// wrap up this story element.
			gameStats.bFinishedInto = true;
			gameStats.bAllowMovement = true;
			gameStats.moveSpeed = 1.0;

			// Reset for next text.
			gameStats.bHasMadeDiedText = false;
			gatekeeperGroup.visible = true;

			let person = makePerson();
			gatekeeperGroup.add(person);
			person.position.x = 10;
			person.position.z = -60;

			scene.remove(activeTextMesh);
			// TODO: also dispose of text elements.
		}
	} else if (around(camera.position.z, -50)
		&& (!gameStats.bFinishedDeathMethod || gameStats.bRequestInteract)) {
		// Stop moving!
		gameStats.bFinishedDeathMethod = false;
		gameStats.bAllowMovement = false;

		if (!nearlyZero(camera.rotation.y)) {
			// Rotate camera towards center.
			let oldRot = camera.rotation.y;
			let newRot = lerp(oldRot, 0, 0.2);
			camera.rotation.y = newRot;
		} else if (!gameStats.bHasMadeDiedText) {
			// Show You Died text and hide people.
			gameStats.bHasMadeDiedText = true;
			gatekeeperGroup.visible = false;

			makeText(deathMethodText.text, text => {
				// Save in global variable for later access.
				activeTextMesh = text;
			});
		} else if (activeTextMesh.position.x > -deathMethodText.length) {
			// Animate shown text.
			let oldPos = activeTextMesh.position.x;
			let newPos = oldPos - (0.25 * textScrollRate);
			activeTextMesh.position.x = newPos;
		} else {
			// wrap up this story element.
			gameStats.bAllowMovement = true;
			gameStats.moveSpeed = 1.0;
			gameStats.bFinishedDeathMethod = true;
			// Reset for next text.
			gameStats.bHasMadeDiedText = false;
			gatekeeperGroup.visible = true;

			scene.remove(activeTextMesh);
			// TODO: also dispose of text elements.
		}
	}

	// render scene into target
	renderer.setRenderTarget(target);
	renderer.render(scene, camera);

	// render post FX
	renderer.setRenderTarget(postFXTarget);
	renderer.render(postScene, postCamera);

	// Don't use a render target - render actually onto the visible canvas.
	renderer.setRenderTarget(null);
	renderer.render(presentationScene, presentationCamera);

	// Reset for next frame.
	gameStats.bRequestInteract = false;

}

function makeText(text, callback) {
	// just in case, hide previous text
	if (activeTextMesh != null) activeTextMesh.visible = false;

	var loader = new THREE.FontLoader();
	loader.load("subway_ticker_regular.json", function (font) {
		var geometry = new THREE.TextGeometry(text, {
			font: font,
			size: 4,
			height: 0.1,
			curveSegments: 12,
			bevelEnabled: true,
			bevelThickness: 0.125,
			bevelSize: 0.125,
			bevelSegments: 1
		});
		var material = new THREE.MeshBasicMaterial({ color: 0x00ff00 });
		var mesh = new THREE.Mesh(geometry, material);
		scene.add(mesh);
		mesh.position.x = camera.position.x + 3;
		mesh.position.y = 3;
		mesh.position.z = camera.position.z - 10;
		callback(mesh);
	});
}

// Input handlers
//#region input handlers
function onForwardPressed() { if (moveTarget.x == 0) moveTarget.x = 1; }
function onBackwardPressed() { if (moveTarget.x == 0) moveTarget.x = -1; }
function onLeftPressed() { if (moveTarget.y == 0) moveTarget.y = -1; }
function onRightPressed() { if (moveTarget.y == 0) moveTarget.y = 1; }
function onInteractPressed() { gameStats.bRequestInteract = true; }

function onForwardReleased() { if (moveTarget.x == 1) moveTarget.x = 0; }
function onBackwardReleased() { if (moveTarget.x == -1) moveTarget.x = 0; }
function onLeftReleased() { if (moveTarget.y == -1) moveTarget.y = 0; }
function onRightReleased() { if (moveTarget.y == 1) moveTarget.y = 0; }
function onInteractReleased() {/* Gets reset by `animate` function. */ }

// Keyboard bindings
(() => {
	document.addEventListener("keydown",
		event => {
			switch (event.key) {
				case "w": onForwardPressed(); break;
				case "s": onBackwardPressed(); break;
				case "a": onLeftPressed(); break;
				case "d": onRightPressed(); break;
				case "e": onInteractPressed(); break;
			}
		}
	);

	document.addEventListener("keyup",
		event => {
			switch (event.key) {
				case "w": onForwardReleased(); break;
				case "s": onBackwardReleased(); break;
				case "a": onLeftReleased(); break;
				case "d": onRightReleased(); break;
				case "e": onInteractReleased(); break;
			}
		}
	);
})();

// Screen button bindings
(() => {
	var buttons = document.querySelectorAll(".game-controls--button");
	buttons.forEach(button => {
		var btnIdx = button.getAttribute("data-game-button-index");
		if (btnIdx != null) {
			var btnPress = (btnIdx) => {
				switch (btnIdx) {
					case "8": onForwardPressed(); break;
					case "2": onBackwardPressed(); break;
					case "4": onLeftPressed(); break;
					case "6": onRightPressed(); break;
					case "5": onInteractPressed(); break;
				}
			};

			var btnRelease = (btnIdx) => {
				switch (btnIdx) {
					case "8": onForwardReleased(); break;
					case "2": onBackwardReleased(); break;
					case "4": onLeftReleased(); break;
					case "6": onRightReleased(); break;
					case "5": onInteractReleased(); break;
				}
			};

			// mouse events
			button.addEventListener("mousedown", () => { btnPress(btnIdx); });
			button.addEventListener("mouseup", () => { btnRelease(btnIdx); });
			button.addEventListener("mouseleave", () => { btnRelease(btnIdx); });

			// touch events
			button.addEventListener("touchstart", () => { btnPress(btnIdx); });
			button.addEventListener("touchend", () => { btnRelease(btnIdx); });
			button.addEventListener("touchmove", event => {
				// Simulate a touch leave event. (not supported yet)
				var touch = event.touches[0];
				var el = document.elementFromPoint(touch.clientX, touch.clientY);
				if (el !== button) {
					btnRelease(btnIdx);
				}
			});
		}
	});
})()
//#endregion

// Screen zooming factor

function onToggleZoom() {
	var height = renderer.getSize().y;
	var newScale = 0;

	if (height == nokiaRTSize.y) {
		// Nokia native scale to double nokia scale.
		renderer.setSize(nokiaRTSize.x * 2, nokiaRTSize.y * 2);
		newScale = 2;

	} else if (height == nokiaRTSize.y * 2) {
		// Double nokia size to screen size.
		fitToScreen(true);
		return;

	} else if (height == myScreenSize.y) {
		// Screen size to nokia native scale.
		renderer.setSize(nokiaRTSize.x, nokiaRTSize.y);
		newScale = 1;
	}


	var label = document.getElementById("nokia-scale-size-label");
	if (label != null) label.innerText = "Scale: " + newScale + "x";
}

function getMaxPresentationSize() {
	var ratio = nokiaRTSize.x / nokiaRTSize.y;
	var min = Math.min(window.innerWidth, window.innerHeight);
	min -= 50; //always give some padding
	if (min == window.innerWidth) {
		// Width is smallest.
		return new THREE.Vector2(min, min / ratio);
	}
	// Height is smallest
	return new THREE.Vector2(min, min / ratio);
}

function fitToScreen(force = false) {
	var newScreenSize = getMaxPresentationSize();
	var newScale;
	var height = renderer.getSize().y;
	if (force || height == myScreenSize.y) {
		// Zoom mode set to using last max size.
		renderer.setSize(newScreenSize.x, newScreenSize.y);
		myScreenSize = newScreenSize;
		newScale = newScreenSize.x / nokiaRTSize.x;

		var label = document.getElementById("nokia-scale-size-label");
		if (label != null) label.innerText = "Scale: " + newScale.toFixed(2) + "x";
	}
}

// Screen resize bindings and initial state.
(() => {
	window.addEventListener("resize", () => { fitToScreen(false); });
	fitToScreen();
	onToggleZoom();
	onToggleZoom();
})();

// make a person
function makePerson() {
	var group = new THREE.Group();

	var material = new THREE.MeshBasicMaterial({ color: 0xffff00 });

	var geometry, mesh;

	// body
	geometry = new THREE.BoxBufferGeometry(3, 5.5, 2);
	mesh = new THREE.Mesh(geometry, material);
	mesh.position.y = 1;
	group.add(mesh);

	// head
	geometry = new THREE.SphereGeometry(2, 32, 32);
	mesh = new THREE.Mesh(geometry, material);
	mesh.position.y = 6;
	group.add(mesh);

	// left arm
	geometry = new THREE.BoxBufferGeometry(1, 3, 2);
	mesh = new THREE.Mesh(geometry, material);
	mesh.position.x = 2.5;
	mesh.position.y = 2.5;
	group.add(mesh);

	// right arm
	mesh = new THREE.Mesh(geometry, material);
	mesh.position.x = -2.5;
	mesh.position.y = 2.5;
	group.add(mesh);

	return group;
}

// populate room
(() => {
	gatekeeperGroup = new THREE.Group();
	var numPl = 3, gap = 10;
	var initGap = -(numPl * gap) / 2;
	for (let i = 0; i < numPl; i++) {
		let person = makePerson();
		person.position.x = (i * gap) + initGap;
		gatekeeperGroup.add(person);
	}
	scene.add(gatekeeperGroup);
})();
