## My game

Live demo:
[https://fruity-games.gitlab.io/nokia-3310-2/](https://fruity-games.gitlab.io/nokia-3310-2/)

## Resources Used
* Three.js game engine:
[https://threejs.org/](https://threejs.org/)

* Dither shader effect:
[https://github.com/hughsk/glsl-dither](https://github.com/hughsk/glsl-dither)

* Depth rendering shader, adapted from:
[https://github.com/mrdoob/three.js/blob/master/examples/webgl_depth_texture.html](https://github.com/mrdoob/three.js/blob/master/examples/webgl_depth_texture.html)

* Font, adapted from:
[https://ff.static.1001fonts.net/s/u/subway-ticker.regular.ttf](https://ff.static.1001fonts.net/s/u/subway-ticker.regular.ttf)